<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paging extends CI_Controller {
    public function __construct()
  {
        parent::__construct();

        // load Pagination library
        $this->load->library('pagination');

        // load URL helper
        $this->load->helper('url');
    }

    public function index()
    {
        // load db and model
        $this->load->database();
        $this->load->model('Users');

        // init params
        $params = array();

        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->Users->get_total();

        //load config file
        $this->config->load('pagination',TRUE);
        $settings = $this->config->item('pagination');
        $settings['total_rows'] = $total_records;
        $settings['base_url'] = base_url() . 'paging/index';

        if ($total_records > 0)
          {
              // get current page records
              $params["results"] = $this->Users->get_current_page_records($settings['per_page'], $start_index);

              // use the settings to initialize the library
              $this->pagination->initialize($settings);

              // build paging links
              $params["links"] = $this->pagination->create_links();
          }

          $this->load->view('user_listing', $params);
        }
  }
?>
