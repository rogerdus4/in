<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guzzle_client_test extends CI_Controller {

    public function index()
    {
        $php_required = '5.5';
        $this->load->helper('url');

        $result = null;
        $status_code = null;
        $content_type = null;

        $code_example = <<<EOT

        \$user_id = 1;

        \$this->load->helper('url');

        \$client = new GuzzleHttp\Client();
        \$res = \$client->get(
            site_url('api/example/users/id/'.\$user_id.'/format/json'),
            [
                'auth' =>  ['admin', '1234'],
                'verify' => false,  // Disable SSL verification, this option value is insecure and should be avoided!
            ]
        );

        \$result = (string) \$res->getBody();
        \$status_code = \$res->getStatusCode();

        \$content_type = \$res->getHeader('content-type');
        \$content_type = is_array(\$content_type) ? \$content_type[0] : \$content_type;

EOT;

        if (is_php($php_required))
        {
            eval($code_example);
        }

        $this->load->view('guzzle_client_test', compact('php_required', 'code_example', 'result', 'status_code', 'content_type'));
    }
    public function postGuzzleRequest()
    {
        $this->load->helper('url');

        $datosDelFormulario ['idAlterna']="1";
        $datosDelFormulario ['idFuente']="15";
        $datosDelFormulario ['averCarpetaNuc']='ECA/ECA/EC2/034/039443/18/02';
        $datosDelFormulario ['noOficio']='13602018';
        $datosDelFormulario ['fechaDocto']='2018-03-07';
        $datosDelFormulario ['horaDocto']='15:16';
        $datosDelFormulario ['agenteDocto']='ADMINISTRADOR';
        $datosDelFormulario ['placa']='HH7494A';
        $datosDelFormulario ['permiso']='';
        $datosDelFormulario ['serie']='3N6AD35C7JK852933';
        $datosDelFormulario ['nomotor']='QR25214254H';
        $datosDelFormulario ['modelo']='2018';
        $datosDelFormulario ['idMarca']='3';
        $datosDelFormulario ['idSubmarca']='805';
        $datosDelFormulario ['idTipoVehiculo']='45';
        $datosDelFormulario ['idColor']='86';
        $datosDelFormulario ['senas']='MN';
        $datosDelFormulario ['idEntidadRecupera']='15';
        $datosDelFormulario ['idMunicipioRecupera']='857';
        $datosDelFormulario ['fechaRecuperacion']='2017-12-05';
        $datosDelFormulario ['horaRecuperacion']='15:40';
        $datosDelFormulario ['idEntidadEntrega']='15';
        $datosDelFormulario ['idMunicipioEntrega']='857';
        $datosDelFormulario ['fechaEntregado']='2017-12-05';
        $datosDelFormulario ['horaEntregado']='15:30';
        $datosDelFormulario ['nombreEntrega']='Pedro';
        $datosDelFormulario ['apellidoPaternoEntrega']='Paramo';
        $datosDelFormulario ['apellidoMaternoEntrega']='M';
        $datosDelFormulario ['urlAnexo']="fgjem.edomex.gob.mx/sis/DocumentoBaja.pdf";

        $client = new \GuzzleHttp\Client(['headers' => [ 'Content-Type' => 'application/json']]);
        $url = 'http://187.174.226.181/WSPGJEMVehiculos/registroVehicular' ;
/*        $info = array(
          "username" => 'PGJEM$45',
          "password" => 'Veh1#358',
          "infoVehiculo" => $datosDelFormulario);
*/

        $response = $client->post('http://187.174.226.181/WSPGJEMVehiculos/registroVehicular',
            ['body' =>
                [
                  'username'  => 'PGJEM$45'
                ],
                [
                  'password'  => 'Veh1#358'
                ]
            ]
        );

        $response = $request->send();
        var_dump($response);
        dd($response);

    }

}
