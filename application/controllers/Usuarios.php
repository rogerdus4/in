<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Usuarios extends CI_Controller{

        public function __construct()
        {
            parent::__construct();
            $this->load->library('curl');
            $this->load->library('pagination');
            $this->load->helper('url');
            $this->load->library('session');
            $this->load->library('encrypt');
        }

        public function enc_password($psw){
          $opciones = [
              'cost' => 12,
          ];

          $psw = password_hash($psw,PASSWORD_BCRYPT,$opciones);
          return $psw;
        }


        public function ConsultaUsuario(){
          $this->load->view('Header');
          $this->load->view('menu_all');
          $this->load->view('ConsultaUsuario');
        }

        public function AgregarUsuario(){
          //$data['datos']=$result;
          $data['fiscalia']=$this->Fiscalia();
          $data['tipo']=$this->Tipo();

          $this->load->view('Header');
          $this->load->view('menu_all');
          $this->load->view('AgregarUsuario',$data);

        }

        public function Fiscalia(){
          $ruta = $this->config->site_url().'api/Usuarios_api/cat_fiscalia';
          return json_decode(file_get_contents($ruta));
        }

        public function Agencia($fiscalia){
          $arreglo = array('data'=>$fiscalia);
          $this->curl->create($this->config->site_url().'api/Usuarios_api/cat_agencia');
          $this->curl->post($arreglo);
          $result = json_decode($this->curl->execute());
          return $result;
        }

        public function Tipo(){
          $ruta = $this->config->site_url().'api/Usuarios_api/cat_tipo';
          return json_decode(file_get_contents($ruta));
        }

        public function NuevoUsuario(){
          $nombre   = $this->input->post('nombre');
          $nombre   = strtoupper($nombre);
          $paterno  = $this->input->post('paterno');
          $paterno  = strtoupper($paterno);
          $materno  = $this->input->post('materno');
          $materno  = strtoupper($materno);
          $telefono = $this->input->post('telefono');
          $correo   = $this->input->post('correo');
          $password = $this->input->post('psw');
            $password = $this->enc_password($password);
          $fiscalia = $this->input->post('fiscalia');
          $agencia  = $this->input->post('agencia');
          $tipo     = $this->input->post('tipo');

          $array = array('nombre' =>$nombre, 'paterno' =>$paterno,  'materno' =>$materno, 'telefono' =>$telefono,
          'correo' =>$correo, 'password' =>$password, 'fiscalia' =>$fiscalia, 'agencia' =>$agencia, 'tipo' =>$tipo);
          $this->curl->create($this->config->site_url().'api/Usuarios_api/InsertaUsuario');
          $this->curl->post($array);
          $result = json_decode($this->curl->execute());

          if ($result) {
            $data['datos']= $result;
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario',array('mensaje' =>'Usuario creado correctamente'));
          }else{
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario',array('mensaje' =>'El usuario debe existir o a tenido un error'));
          }

        }//public function NuevoUsuario

        public function MostrarInformacionUsuario(){
          $nombre = $this->input->post('nombre');
          $nombre = strtoupper($nombre);
          $id_agencia = $this->session->userdata('id_agencia');
          $array = array('nombre'=>$nombre,'id_agencia'=>$id_agencia);
          $this->curl->create($this->config->site_url().'/api/Usuarios_api/consulta_usuario');
          $this->curl->post($array);
          $result= json_decode($this->curl->execute());

          if ($result) {
            $data['datos']= $result;

            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario');
            $this->load->view('TablaUsuario',$data);
          }else{
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario',array('mensaje' =>'No se Encontro Información'));
          }
        }//public function MostrarInformacionUsuario

        public function Actualizar(){
          $mas = $this->input->post('id');
          $array = array('info'=>$mas);
          $this->curl->create($this->config->site_url().'api/Usuarios_api/mas_Informacion_Usuario');
          $this->curl->post($array);
          $result= json_decode($this->curl->execute());

          if ($result) {

            $fiscalia = $result['0']->id_fiscalia;
            //var_dump($result);
            $data['datos'] = $result;
            $data['fiscalias']=$this->Fiscalia();
            $data['agencias']=$this->Agencia($fiscalia);
            $data['tipos']=$this->Tipo();
            //$this->load->view('HeaderModal');
            $this->load->view('validarUsuarios',$data);
          }else{
            $this->load->view('HeaderModal');
            $this->load->view('validarUsuarios',array('mensaje' => 'No se encontro Informacion'));
          }
        }

        public function ModificaUsuario(){
          $id = $this->input->post('id');
          $nombre   = $this->input->post('nombree');
          $nombre   = strtoupper($nombre);
          $paterno  = $this->input->post('paterno');
          $paterno  = strtoupper($paterno);
          $materno  = $this->input->post('materno');
          $materno  = strtoupper($materno);
          $telefono = $this->input->post('telefono');
          $correo   = $this->input->post('correo');
            //$password = $this->enc_password('123456');
          $fiscalia = $this->input->post('fiscalia');
          $agencia  = $this->input->post('agencia');
          $tipo     = $this->input->post('tipo');

          //consulta de usuario para comparar diferencias de agencia
          $array = array('nombre' => $nombre,'agencia' => $agencia);
          $this->curl->create($this->config->site_url().'/api/Usuarios_api/consulta_usuario');
          $this->curl->post($array);
          $resul = json_decode($this->curl->execute());

              if($resul){
                //si la agencia es diferente manda a llamar al procedimiento de la agencia
              //  var_dump($resul);
                $agencia_a = $resul[0]->id_agencia;

                if($agencia != $agencia_a ){
                  $array = array('id'=>$id, 'agencia' =>$agencia, 'tipo' =>$tipo);
                  $this->curl->create($this->config->site_url().'api/Usuarios_api/EditaAgencia');
                  $this->curl->post($array);
                }else {
                  $array = array('id'=>$id,'nombre' =>$nombre, 'paterno' =>$paterno, 'materno' =>$materno, 'telefono' =>$telefono,
                  'correo' =>$correo, 'tipo' =>$tipo);
                  $this->curl->create($this->config->site_url().'api/Usuarios_api/EditaUsuario');
                  $this->curl->post($array);
                }
              }

          $result =json_decode($this->curl->execute());

          if ($result) {
            $data['datos']= $result;
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario');
          }else{
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario',array('mensaje' =>'Error al modificar a Usuario'));
          }
        }//fin modificar Usuario

        public function EliminaUsuario($id){

          $array = array('id_usuario' =>$id);
          $this->curl->create($this->config->site_url().'/api/Usuarios_api/DeleteUsuario');
          $this->curl->post($array);
          $result = json_decode($this->curl->execute());

          if ($result) {
            $data['datos']= $result;
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario',array('mensaje' =>'Usuario Eliminado Correctamente'));
          }else{
            $this->load->view('Header');
            $this->load->view('menu_all');
            $this->load->view('ConsultaUsuario',array('mensaje' =>'Error al Eliminar a Usuario'));
          }
        }//fin EliminaUsuario

        public function EstadisticaSupervisor(){

          if($this->input->post('desde')  && $this->input->post('hasta')){
              $desde = $this->input->post('desde');
              $hasta = $this->input->post('hasta');
              $id_agencia = $this->session->userdata('id_agencia');
              $array = array('id_agencia' =>$id_agencia);

              //Consulta a los supervisores y trae su nombre y el id
              $this->curl->create($this->config->site_url().'/api/Usuarios_api/consulta_tipo');
              $this->curl->post($array);
              $resul= json_decode($this->curl->execute());

              $array = array('desde' => $desde, 'hasta' =>$hasta);

              $this->load->view('Header');
              $this->load->view('menu_all');
              $this->load->view('head_tabla',$array);

              if ($resul) {
                  ///envio a la consulta de sus asignados
                  foreach ($resul as $info) {

                    $id_usuario = $info->id_usuario;
                    $array = array('id_usuario' =>$id_usuario,'desde' => $desde, 'hasta' =>$hasta);
                    $this->curl->create($this->config->site_url().'/api/Documentacion_api/Estadistica_Supervisor');
                    $this->curl->post($array);
                    $result= json_decode($this->curl->execute());
                    $data['datos'] = $result;
                    //envio de datos a la vista
                    $this->load->view('EstadisticaUsuarios',$data);
                  }

              }else{
                $this->load->view('Principal',array('mensaje' =>'No se Encontro Información'));
              }
        }
        else{
          $this->load->view('Header');
          $this->load->view('menu_all');
          $this->load->view('PeriodoSupervisor');
        }

      }///fin function EstadisticaSupervisor

    }
