<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Controller_curl extends CI_Controller {
public function __construct() {
  parent::__construct();
}
public function index(){

  //  Calling cURL Library
  $this->load->library('curl');

  //  Setting URL To Fetch Data From
  $this->curl->create('https://www.formget.com/');

  //  To Temporarily Store Data Received From Server
  $this->curl->option('buffersize', 10);

  //  To support Different Browsers
  $this->curl->option('useragent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)');

  //  To Receive Data Returned From Server
  $this->curl->option('returntransfer', 1);

  //  To follow The URL Provided For Website
  $this->curl->option('followlocation', 1);

  //  To Retrieve Server Related Data
  $this->curl->option('HEADER', true);

  //  To Set Time For Process Timeout
  $this->curl->option('connecttimeout', 600);

  //  To Execute 'option' Array Into cURL Library & Store Returned Data Into $data
  $data = $this->curl->execute();

  //  To Display Returned Data
  echo $data;
}

public function validar(){
  //  Calling cURL Library
  $this->load->library('curl');

   $datosDelFormulario ['idAlterna']="1";
   $datosDelFormulario ['idFuente']="15";
   $datosDelFormulario ['averCarpetaNuc']='NEZ/FRO/VNE/062/053251/18/03';
   $datosDelFormulario ['noOficio']='07052018';
   $datosDelFormulario ['fechaDocto']='2018-03-13';
   $datosDelFormulario ['horaDocto']='15:16';
   $datosDelFormulario ['agenteDocto']='ROGELIO';
   $datosDelFormulario ['placa']='1C9LR';
   $datosDelFormulario ['permiso']='';
   $datosDelFormulario ['serie']='LLCLT1P05ECK09832';
   $datosDelFormulario ['nomotor']='LC152QMIE6625771';
   $datosDelFormulario ['modelo']='2014';
   $datosDelFormulario ['idMarca']='0';
   $datosDelFormulario ['idSubmarca']='0';
   $datosDelFormulario ['idTipoVehiculo']='0';
   $datosDelFormulario ['idColor']='25';
   $datosDelFormulario ['senas']='MN';
   $datosDelFormulario ['idEntidadRecupera']='15';
   $datosDelFormulario ['idMunicipioRecupera']='58';
   $datosDelFormulario ['fechaRecuperacion']='2018-03-13';
   $datosDelFormulario ['horaRecuperacion']='15:40';
   $datosDelFormulario ['idEntidadEntrega']='15';
   $datosDelFormulario ['idMunicipioEntrega']='58';
   $datosDelFormulario ['fechaEntregado']='2018-03-13';
   $datosDelFormulario ['horaEntregado']='15:30';
   $datosDelFormulario ['nombreEntrega']='EDUARDO';
   $datosDelFormulario ['apellidoPaternoEntrega']='MANZO';
   $datosDelFormulario ['apellidoMaternoEntrega']='ITURBIDE';
   $datosDelFormulario ['urlAnexo']="fgjem.edomex.gob.mx/sis/DocumentoBaja.pdf";
   $datosDelFormulario ['idUsuario']='3';

  $info = array(
    "username" => 'PGJEM$45',
    "password" => 'Veh1#358',
    "infoVehiculo" => json_encode($datosDelFormulario));
    //  To support Different Browsers
    $this->curl->option('useragent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)');
   //   To Set Time For Process Timeout
  //  $this->curl->option('connecttimeout', 600);
    //$this->curl->create('http://187.174.226.181/WSPGJEMVehiculos/registroVehicular'); //pruebas
    $this->curl->create('http://201.144.252.134/WSPGJEMVehiculos/registroVehicular'); //produccion
    //$this->curl->create('http://10.10.0.63:8080/WSVehiculosRE/registroVehicular');
    $this->curl->post($info);

  //  To Receive Data Returned From Server
  $this->curl->option('returntransfer', 1);

  //  To follow The URL Provided For Website
  $this->curl->option('followlocation', 1);

  //  To Retrieve Server Related Data
  $this->curl->option('HEADER', true);

  //  To Set Time For Process Timeout
//  $this->curl->option('connecttimeout', 600);


    $data = $this->curl->execute();

    echo $data;
  //var_dump($info);
  }

public function show(){

  $datosDelFormulario ['idAlterna']="1";
  $datosDelFormulario ['idFuente']="15";
  $datosDelFormulario ['averCarpetaNuc']='ECA/ECA/EC2/034/039443/18/02';
  $datosDelFormulario ['noOficio']='13602018';
  $datosDelFormulario ['fechaDocto']='2018-03-07';
  $datosDelFormulario ['horaDocto']='15:16';
  $datosDelFormulario ['agenteDocto']='ROGELIO';
  $datosDelFormulario ['placa']='HH7494A';
  $datosDelFormulario ['permiso']='NA';
  $datosDelFormulario ['serie']='3N6AD35C7JK852933';
  $datosDelFormulario ['nomotor']='QR25214254H';
  $datosDelFormulario ['modelo']='2018';
  $datosDelFormulario ['idMarca']='3';
  $datosDelFormulario ['idSubmarca']='805';
  $datosDelFormulario ['idTipoVehiculo']='45';
  $datosDelFormulario ['idColor']='86';
  $datosDelFormulario ['senas']='MN';
  $datosDelFormulario ['idEntidadRecupera']='5';
  $datosDelFormulario ['idMunicipioRecupera']='4';
  $datosDelFormulario ['fechaRecuperacion']='2018-03-07';
  $datosDelFormulario ['horaRecuperacion']='15:40';
  $datosDelFormulario ['idEntidadEntrega']='15';
  $datosDelFormulario ['idMunicipioEntrega']='857';
  $datosDelFormulario ['fechaEntregado']='2018-03-07';
  $datosDelFormulario ['horaEntregado']='15:30';
  $datosDelFormulario ['nombreEntrega']='NR FINANCE';
  $datosDelFormulario ['apellidoPaternoEntrega']='MEXICO';
  $datosDelFormulario ['apellidoMaternoEntrega']='SA DE CV SOFOM E.R';
  $datosDelFormulario ['urlAnexo']="fgjem.edomex.gob.mx/sis/DocumentoBaja.pdf";
  $datosDelFormulario ['idUsuario']='3';

      Requests::register_autoloader();
    $headers = array('Accept' => 'application/json');
      $info = array(
      "username" => 'PGJEM$45',
      "password" => 'Veh1#358',
      "infoVehiculo" => json_encode($datosDelFormulario));
    //$request = Requests::get('https://api.github.com/gists', $headers, $options);
    //$this->curl->create('http://187.174.226.181/WSPGJEMVehiculos/registroVehicular'); //pruebas
    //$this->curl->create('http://10.10.2.136:8080/WSVehiculosRE/registroVehicular');
    //$this->curl->create('http://201.144.252.134/WSPGJEMVehiculos/registroVehicular'); // produccion

    $request = Requests::post('http://187.174.226.181/WSPGJEMVehiculos/registroVehicular', $headers, $info); //pruebas
    //$request = Requests::post('http://201.144.252.134/WSPGJEMVehiculos/registroVehicular', $headers, $info); //produccion

    var_dump($request->status_code);
    // int(200)

    var_dump($request->headers['content-type']);
    // string(31) "application/json; charset=utf-8"

    var_dump($request);

    //var_dump($info);
  }

}
?>
